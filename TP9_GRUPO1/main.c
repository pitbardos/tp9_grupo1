
/* 
 * File:   main.c
 * Author: grupo1
 *
 * Created on May 13, 2022, 6:51 PM
 */

#include <stdio.h>
#include <stdint.h>
#include "puertos.h"

enum states {LED_ON = 0x0100, NO_VALID,START, ALL_INV = 't', ALL_OFF = 'c',ALL_ON = 's',QUIT = 'q'};
/* Dependiendo de la entrada el programa ejecuta diferentes acciones, representadas por estados
 * LED ON: Encendido individual de leds. Es distinto a cualquier caracter ingresable por el usuario
 * NO_VALID: Entrada no valida por exceso de caracteres ingresados. Es distinto a cualquier caracter ingresable por el usuario
 * START: Valor inicial para el estado. Es distinto a cualquier caracter ingresable por el usuario
 * ALL_INV: Inversion de el estado de todos los leds
 * ALL_OF: Apagado de todos los leds
 * ALL_ON: Encendido de todos los leds
 * QUIT: Fin de la ejecucion por deseo del usuario
 */
#define ENCENDIDO 'O'       //Caracter designado para ser mostrado como LED ENCENDIDO
#define APAGADO '-'         //Caracter designado para ser mostrado como LED APAGADO
#define ISBIT(x) (((x) >= '0') && ((x) <= '7')) //Devuelve un 1 si el caracter se encuentra entre 0 y 7, caso contrario 0.
#define CHAR2NUM(x) ((x) - '0')                 //Convierte un caracter entre '0' y '7' a un numero entre 0 y 7
#define MASK8BITS 0xFF //Mascara de ocho 1s
#define BIENVENIDA "Bienvenido al programa de manejo de LEDS\nIngrese un numero del 0 al 7 para encender el LED correspondiente\nIngrese t para invertir el estado de todos los leds\nIngrese c para apagar todos los leds\nIngrese s para encender todos los leds\nIngrese q para salir\n"

void portPrint(char port);  //Muestra en pantalla el valor del puerto. Recibe un char (puerto)

int main(){	
    maskOff(PORTA,MASK8BITS);  //Se inicializa el puerto A en 0, es decir, comienzan apagados
    printf(BIENVENIDA); //Se imprime el mensaje de bienvenida
    int state=START;  //Entero que indica el estado de la funcion segun el caracter recibido. Al comenzar en START, es distinto de q, por lo que siempre permite un primer input del usuario
    int charinput;  //Entero que almacena el caracter ingresado por el usuario
    
    int charcount;  //Entero que almacena la cantidad de caracteres ingresada por el usuario en una sola linea 
    while(state != QUIT){   //Loop hasta que el usuario desee salir
        
        portPrint(PORTA);     //Se imprime el estado del puerto A
        charinput = getchar();  //Se toma el primer caracter de la entrada
        charcount = 1;  //Se inicializa el contador de caracteres en 1
        
        while(charinput != '\n' && getchar() != '\n'){  
            //Se cuenta la cantidad de caracteres hasta que se encuentre un \n en el buffer si la entrada es cualquier caracter a excepcion del \n
            charcount++;
        }//notar que este while limpia el buffer del getchar

        if(charcount == 1){ //Cantidad de caracteres ingresados = 1 es condicion necesaria para que la entrada sea valida 
            if(ISBIT(charinput)){   //Si la entrada es un numero del 0 al 7 (bit) entonces el estado pasa a ser LED_ON
                state = LED_ON;
            }
            else{
                state = charinput;  //Si la entrada no es un numero de bit se la asigna al valor de state
                //En esta instancia state podria tomar un estado inexistente (que se descartara en el default)
            }
        }
        else{   //Si la cantidad de caracteres no es 1 entonces la entrada no es valida por exceso de caracteres
            state = NO_VALID;
        }

        switch(state){  //Dependiendo del valor de state realiza distintas acciones
            case LED_ON:    //Caso encendido de bit individual
                bitSet(PORTA,CHAR2NUM(charinput));    //Se enciende el bit indicado del puerto A
                break;
            case ALL_INV:   //Caso inversion de todos los leds
                maskToggle(PORTA, MASK8BITS);   //Se ejecuta maskToggle en todos los bits del puerto A
                break;
            case ALL_OFF:   //Caso apagado de todos los leds
                maskOff(PORTA, MASK8BITS);  //Se ejecuta maskOff en todos los bits del puerto A
                break;
            case ALL_ON:    //Caso encendido de todos los led
                maskOn(PORTA, MASK8BITS);//Se ejecuta mmaskOn en todos los bits del puerto A
                break;
            case QUIT:      //Caso fin de ejecuion --> Sale del switch. Al volver a entrar al while, el valor de estado sera QUIT y no entrara nuevamente en el bucle
                break;
            case NO_VALID:  //Caso entrada no valida 
            default:        //Caso caracter desconocido
                printf("Entrada invalida\n");   //Se imprime un mensaje de error
        }
    }
    return 0; 
}



//Esta funcion recibe como parametro un dato de 8 bits y se encarga de imprimir bit por bit en pantalla: si el bit esta encendido, imprime una O; y si esta apagado imprime un -

void portPrint(char port){  //Muestra en pantalla el valor del bit segun los caracteres ENCENDIDO Y APAGADO. Recibe un char (puerto) y el bit a mostrar
    int bit;
    printf("PUERTO %c: ",port); //Imprime el puerto seleccionado
    for( bit = 7; bit >= 0; bit--){ //Se recorren todos los bits del puerto ingresado
        printf("%c", bitGet(port, bit)? ENCENDIDO : APAGADO );  //Lee el valor del bit, si es un 1 imprime el caracter asignado a ENCENDIDO, de lo contrario imprime APAGADO
    }
    printf("\n");   //Imprime un enter
}
