/****************************************************************************************************************************************
                                                        ___                      _              
                                                       | _ \  _  _   ___   _ _  | |_   ___   ___
                                                       |  _/ | || | / -_) | '_| |  _| / _ \ (_-<
                                                       |_|    \_,_| \___| |_|    \__| \___/ /__/

****************************************************************************************************************************************/

/****************************************************************************************************************************************
 * Esta libreria sirve para la simulacion de puertos mediante software.
 * Contiene los siguientes puertos:
 *      -Puerto A: puerto de 8 bits.
 *      -Puerto B: puerto de 8 bits.
 *      -Puerto D: puerto de 16 bits, union de A y B. Donde B es la parte menos significativa de D y A la mas significativa.
 * Contiene varias funciones que permiten modificar y leer estos puertos, estas funciones estan explicadas mas adelante en este archivo.
****************************************************************************************************************************************/

#ifndef PUERTOS_H
#define PUERTOS_H

/********************************************************************************************************************
 *                                                  Constantes
********************************************************************************************************************/
#define PORTA   'A'//constantes para que el usuario referencie a los puertos
#define PORTB   'B'
#define PORTD   'D'

/********************************************************************************************************************
********************************************************************************************************************/

/********************************************************************************************************************
 *                                              Prototipos de funciones
********************************************************************************************************************/

void bitSet(char port, int bit);//Prende el bit indicado del puerto seleccionado.

void bitClr(char port, int bit);//Apaga el bit indicado del puerto seleccionado.

int bitGet(char port, int bit);//Obtiene el valor del bit indicado del puerto seleccionado.

void bitToggle(char port, int bit);//Invierte el valor del bit indicado del puerto seleccionado.

void maskOn(char port, uint16_t mask);//Prende los bits indicados en la mascara del puerto seleccionado.

void maskOff(char port, uint16_t mask);//Apaga los bits indicados en la mascara del puerto seleccionado.

void maskToggle(char port, uint16_t mask);//Invierte el valor de los bits indicados en la mascara del puerto seleccionado.

/********************************************************************************************************************
********************************************************************************************************************/

#endif /* PUERTOS_H */

