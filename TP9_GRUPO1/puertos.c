#include <stdio.h>
#include <stdint.h>
#include "puertos.h"

#define PUERTOA puerto.AB.A //forma de acceder al puerto A como byte
#define PUERTOB puerto.AB.B //forma de acceder al puerto B como byte
#define PUERTOD puerto.D //forma de acceder al puerto A y B como 2 bytes conjuntos

#define INVALIDO 0
#define VALIDO 1


#define ONEMASK(bit) (1<<(bit)) //genera una mascara con un 1 en el numero de bit indicado, y 0s en el resto de los bits.
#define ZEROMASK(bit) (~ONEMASK(bit)) //genera una mascara con un 0 en el numero de bit indicado, y 1s en el resto de los bits.

#define OFFSET(c) ( (((c) - 'A' + 1) % 2) * 8 )
/*
 * Esta macro genera un offset para modificar el puerto A cuando sea necesario.
 * Para evitar realizar case switch cada vez que se llama a una funcion utilizamos esta macro.
 * Lo que hace es que si el usuario quiere modificar algo del puerto A (byte mas significativo de D), genera un offset de 8 que luego
 * se utiliza en el shifteo del bit que se desea utilizar. De esta forma se modificara el contenido del puerto A.
 * En caso de que sean los puertos B o D el offset es 0.
 */

#define BITSET(puerto, bit) ( (puerto) = (puerto) | (ONEMASK(bit)) )
/*
 * Esta macro pone en 1 el bit seleccionado del puerto indicado.
 * La macro recibe el puerto y el numero de bit a setear.
 * Luego se utiliza la macro anteriormente creada "ONEMASK" para realizar un OR 
 * al puerto con una mascara que contiene un uno en el bit que se desea setear.
 */

#define BITCLR(puerto, bit) ( (puerto) = (puerto) & (ZEROMASK(bit)) )
/*
 * Esta macro pone en 0 el bit seleccionado del puerto indicado.
 * La macro recibe el puerto y el numero de bit a limpiar.
 * Luego se utiliza la macro anteriormente creada "ZEROMASK" para realizar un AND 
 * al puerto con una mascara que contiene un cero en el bit que se desea limpiar.
 */

#define BITGET(puerto, bit) ( ((puerto) & (ONEMASK(bit))) != 0)
/*
 * Esta macro devuelve el valor del bit seleccionado del puerto indicado
 * La macro recibe el puerto y el numero de bit a obtener.
 * Luego se utiliza la macro anteriormente creada "ONEMASK" para realizar un AND 
 * al puerto con una mascara que contiene un uno en el bit que se desea obtener.
 * Si habia un 1 en el bit a obtener entonces la operacion AND devuelve algo distinto de 0
 * Por lo que BITGET devuelve 1 pues 1 es distinto de 0
 * Si habia un 0 en el bit a obtener entonces la operacion AND devuelve 0
 * Por lo que BITGET devuelve 0 pues 0 no es distinto de 0
 * Aclaracion: esta macro no modifica al puerto, como si lo hacen BITSET y BITCLR
 */

#define BITTOGGLE(puerto, bit) ( BITGET(puerto, bit) ? BITCLR(puerto, bit) : BITSET(puerto, bit) )
/*
 * Esta macro niega el valor del bit seleccionado. Si estaba en 0 pasa a valer uno y viceversa.
 * La macro recibe el puerto y el numero de bit a negar.
 * Luego se utiliza la macro anteriormente creada "BITGET" para saber si el bit se encuentra en 0 o 1
 * si se encuentra en 1, utiliza la macro "BITCLR" para colocarlo en 0.
 * si se encuentra en 0, utiliza la macro "BITSET" para colocarlo en 1.
 */


#define MASKON(puerto, mask) ( (puerto) = (puerto) | (mask) )
/*
 * Esta macro pone en 1 los bits indicados por una mascara del puerto seleccionado.
 * La macro recibe el puerto y la mascara que se desea aplicar.
 * Luego se realiza un OR del puerto con la mascara para encender los bits indicados.
 */

#define MASKOFF(puerto, mask) ( (puerto) = (puerto) & ~(mask) )
/*
 * Esta macro pone en 0 los bits indicados por una mascara del puerto seleccionado.
 * La macro recibe el puerto y la mascara que se desea aplicar.
 * Luego se realiza un AND del puerto con la mascara negada para apagar los bits indicados.
 * Notar que con los bits no indicados, se realiza un AND entre los bit correspondiente del puerto y un 1, por lo que mantiene el valor del bit correspondiente
 */


#define MASKTOGGLE(puerto,mask) ( (puerto) = ( (puerto) ^ (mask) ) )
/*
 * Esta macro invierte el estado los bits indicados por una mascara del puerto seleccionado.
 * La macro recibe el puerto y la mascara que se desea aplicar.
 * Luego la macro realiza un XOR bit a bit entre el puerto y la mascara 
 * Esto genera que los bits del puerto indicados en la mascara se nieguen. El resto mantiene su estado pues se les hace un XOR con 0.
 */

//Prototipos de funciones privadas de este modulo:
static int validacion(char port, unsigned int bit);//Chequea que el numero de bit ingresado sea valido dependiendo del puerto ingresado, y chequea que el puerto sea valido

static int validacionMask(char port, uint16_t mask);//Chequea que la mascara ingresada sea valida dependiendo del puerto ingresado, y chequea que el puerto sea valido


typedef struct {//tipo de dato de un struct de 2 campos de 1 byte.
    uint8_t B;
    uint8_t A;
} dosBytes_t;

static union {//union para que el puerto D ocupe el mismo espacio de memoria que los puertos A y B juntos.
    uint16_t D;
    dosBytes_t AB;
} puerto;

/*
 * Notar que en el desarrollo de la libreria se opto por manejarse con mascaras para crear funciones mas eficientes y robustas.
 * Es por esto que en ningun momento se referencia al campo AB de la union (y por ende a los campos A y B del struct)
 * En lugar de ser una union, puerto podria entonces simplemente haber sido un uint16 ya que solo referenciamos al puerto D y nos manejamos con offsets y mascaras
 * Se opto por dejar planteado el struct por si eventualmente se quisieran agregar funciones que necesiten referenciar a los puertos por separado
 * y/o les sea mas facil que manejarse con un offset.
 */


static int validacion(char port, unsigned int bit){
/*
 * Esta funcion chequea la validez del puerto y el numero de bit ingresado
 */
    int valido = VALIDO; //por default tomamos validez 1
    
    switch(port){
        //Si es el puerto A o B, el numero de bit ingresado no puede ser superior a 7
        case PORTA:
            //notar que se realiza la misma validacion de bit que para PORTB
        case PORTB:
            if(bit > 7){ //notar que bit es no signado, por lo que no hace falta chequear que sea mayor a 0
                valido = INVALIDO;
            }
            break;
            
        case PORTD: //si es el puerto D, el numero de bit ingresado no puede ser superior a 15
            if(bit > 15){
                valido = INVALIDO;
            }
            break;
            
        default:
            valido = INVALIDO; //cualquier "puerto" distinto de A, B o D no es valido
            break;
    }
    
    return valido; 
}

static int validacionMask(char port, uint16_t mask){
/*
 * Esta funcion chequea la validez del puerto y la mascara ingresadas.
 */
    int valido = VALIDO;
    
    switch(port){
        //Si es el puerto A o B, el contenido de la mascara no puede ser superior a 255
        case PORTA:
            
        case PORTB:
            if(mask > 255){
                valido = INVALIDO;
            }
            break;
            
        case PORTD://Si es el puerto D el contenido de la mascara siempre sera valido pues la mascara es un numero de 16 bits
            valido = VALIDO;
            break;
            
        default:
            valido = INVALIDO; // si se ingreso un "puerto" distinto de A, B o D
            break;
    }
    
    return valido;
}

void bitSet(char port, int bit) {
/*
 * Esta funcion setea en 1 el bit del puerto que indica el usuario.
 * Para realizar esto primero chequea que lo ingresado sea valido y luego utiliza la macro BITSET.
 * Ademas utiliza el offset por si se desea modificar unicamente el puerto A, en cuyo caso, OFFSET
 * tomara un valor de 8 y se shiftea la mascara utilizada 8 lugares hacia la izquierda.
 */
    if(validacion(port, bit)==VALIDO){
        BITSET(PUERTOD, bit + OFFSET(port));
    }
    else{
        printf("Por favor ingrese un puerto y un numero de bit validos\n");
    }
}

void bitClr(char port, int bit) {
/*
 * Esta funcion setea en 0 el bit del puerto que indica el usuario.
 * Para realizar esto primero chequea que lo ingresado sea valido y luego utiliza la macro BITCLR.
 * Ademas utiliza el offset por si se desea modificar unicamente el puerto A, en cuyo caso, OFFSET
 * tomara un valor de 8 y se shiftea la mascara utilizada 8 lugares hacia la izquierda.
 */
    if(validacion(port, bit)==VALIDO){
        BITCLR(PUERTOD, bit + OFFSET(port));
    }
    else{
        printf("Por favor ingrese un puerto y un numero de bit validos\n");
    }
}

int bitGet(char port, int bit) {
/*
 * Esta funcion devuelve el valor del bit del puerto que indica el usuario.
 * Para realizar esto primero chequea que lo ingresado sea valido y luego utiliza la macro BITGET.
 * Ademas utiliza el offset por si se desea leer unicamente el puerto A, en cuyo caso, OFFSET
 * tomara un valor de 8 y se shiftea la mascara utilizada 8 lugares hacia la izquierda.
 */
    if(validacion(port, bit)==VALIDO){
        return BITGET(PUERTOD, bit + OFFSET(port));
    }
    else{
        printf("Por favor ingrese un puerto y un numero de bit validos\n");
        return -1;
    }
}

void bitToggle(char port, int bit){
/*
 * Esta funcion niega el valor del bit seleccionado. Si estaba en 0 pasa a valer uno y viceversa.
 * Para realizar esto primero chequea que lo ingresado sea valido y luego utiliza la macro BITTOGLE.
 * Ademas utiliza el offset por si se desea modificar unicamente el puerto A, en cuyo caso, OFFSET
 * tomara un valor de 8 y se shiftea la mascara utilizada 8 lugares hacia la izquierda.
 */
    if(validacion(port, bit)==VALIDO){
        BITTOGGLE(PUERTOD, bit + OFFSET(port));
    }
    else{
        printf("Por favor ingrese un puerto y un numero de bit validos\n");
    }    
}


void maskOn(char port, uint16_t mask){
/*
 * Esta funcion enciende los bits indicados en "mask" del puerto seleccionado en "port"
 * Para realizar esto primero chequea que lo ingresado sea valido y luego utiliza la macro MASKON.
 * Ademas utiliza el offset por si se desea modificar unicamente el puerto A, en cuyo caso, OFFESET
 * tomara un valor de 8 y se shiftea la mascara utilizada 8 lugares hacia la izquierda.
 */
    if(validacionMask(port, mask)==VALIDO){
        MASKON(PUERTOD, (mask << OFFSET(port)));
    }
    else{
        printf("Por favor ingrese una mascara valida\n");
    }     
}

void maskOff(char port, uint16_t mask){
/*
 * Esta funcion apaga los bits indicados en "mask" del puerto seleccionado en "port"
 * Para realizar esto primero chequea que lo ingresado sea valido y luego utiliza la macro MASKOFF.
 * Ademas utiliza el offset por si se desea modificar unicamente el puerto A, en cuyo caso, OFFSET
 * tomara un valor de 8 y se shiftea la mascara utilizada 8 lugares hacia la izquierda.
 */
    if(validacionMask(port, mask)==VALIDO){
        MASKOFF(PUERTOD, (mask << OFFSET(port)));
    }
    else{
        printf("Por favor ingrese una mascara valida\n");
    }     
}

void maskToggle(char port, uint16_t mask){
/*
 * Esta funcion invierte el valor de los bits indicados en "mask" del puerto seleccionado en "port"
 * Para realizar esto primero chequea que lo ingresado sea valido y luego utiliza la macro MASKTOGGLE.
 * Ademas utiliza el offset por si se desea modificar unicamente el puerto A, en cuyo caso, OFFSET
 * tomara un valor de 8 y se shiftea la mascara utilizada 8 lugares hacia la izquierda.
 */
    if(validacionMask(port, mask)==VALIDO){
        MASKTOGGLE(PUERTOD, (mask << OFFSET(port)));
    }
    else{
        printf("Por favor ingrese una mascara valida\n");
    }     
}

